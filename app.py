import time
import utils.halo as halo
import utils.twitter as twitter
from datetime import datetime



def retrieve_last_id():
    # update since_id to the last line on initial call
    fileHandle = open ( 'since_ids.txt',"r" )
    lineList = fileHandle.readlines()
    fileHandle.close()
    print('----------')
    print ("The last line value in the since_ids.txt is: ")
    print('----------')
    print('')

    # or simply
    last_id = lineList[-1]
    print(last_id)
    return last_id

def update_since_id_file(tweet_id):
    f = open("since_ids.txt", "a")
    f.write(str(tweet_id) + "\n") 
    f.close()
    print('----------')
    print('"since_ids.txt" Update triggered for: ' + str(tweet_id))
    print('----------')
    print('')
    return True

while True:
    print('')
    print('##############################')
    print("New loop starting...")
    print('##############################')
    print('')

    # check if datetime is noon to post MOTD
    now = datetime.now()
    second = now.second
    minute = now.minute
    hour = now.hour

    create_motd = False

    print("The current hour and minute date:", hour, minute, second)


    # get last id in file
    last_id = retrieve_last_id()

    # fetch tweets since_id
    tweets = twitter.fetch_mentions(last_id)

    # process each tweet
    for tweet in tweets:
        print("incoming tweet",tweet)

        # parse tweet for Halo info
        parsed_tweet = twitter.parse_tweet(tweet)
        
        # hit halo infinite api
        halo_infinite_data_string = halo.fetch_halo_data(parsed_tweet["halo_username"],parsed_tweet["incoming_query_type"])
        print('----------')
        print("Halo Infinite string =>",halo_infinite_data_string)
        print('----------')
        print('')

        # procees data and create tweet
        if halo_infinite_data_string:
            response_tweet = twitter.create_tweet(halo_infinite_data_string,parsed_tweet["screen_name"])
            twitter.post_tweet(response_tweet,parsed_tweet["tweet_id"])
        else:
            print('----------')
            print("Skip this tweet!!!")
            print('----------')
            print('')
    
    # add last_id to since_id
    if(len(tweets)>0):
        new_since_id = tweets[0]["id_str"]
        update_since_id_file(new_since_id)
    else:
        print('----------')
        print("no new tweets to analyze")
        print('----------')
        print('')

    # reloop after 30s
    t = 30
    while t:
        mins, secs = divmod(t, 60)
        timer = '{:02d}'.format(secs)
        print(timer, end="\r")
        time.sleep(1)
        t -= 1
