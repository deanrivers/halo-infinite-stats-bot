import unittest
import sys

# this is needed to import the files/modules
sys.path.append('../')

# halo
from utils.halo import determine_performace
from utils.halo import process_matches_data
from utils.halo import process_service_record_data
from utils.halo import fetch_halo_data

# twitter
from utils.twitter import parse_tweet
from utils.twitter import fetch_mentions
from utils.twitter import create_tweet
from utils.twitter import post_tweet

class TestParseTweet(unittest.TestCase):
    def test_parse_tweet(self):
        dummy_data = {
            "text":"",
            "id_str":"12345",
            "user": {
                "screen_name":"iDeanRivers"
            },
        }

        #---------------------------------------------------
        dummy_data["text"] = "@Halo_Stats_Bot nyke espy:service_record"
        res_1 = parse_tweet(dummy_data)
        self.assertEqual(res_1,{
            "halo_username":"nyke%20espy",
            "incoming_query_type":"service_record",
            "screen_name": "iDeanRivers",
            "tweet_id":"12345",
            "season":1
        })

        #---------------------------------------------------
        dummy_data["text"] = "This is a tweet with the account at the end @Halo_Stats_Bot"
        res_2 = parse_tweet(dummy_data)
        self.assertEqual(res_2,{
            "halo_username":"",
            "incoming_query_type": None,
            "screen_name": "iDeanRivers",
            "tweet_id":"12345",
            "season":1
        })

        #---------------------------------------------------
        # dummy_data["text"]  = "@Halo_Stats_Bot A random tweet to the bot"
        # res_3 = parse_tweet(dummy_data)
        # self.assertEqual(res_3,{
        #     "halo_username":"sdfgds",
        #     "incoming_query_type": None,
        #     "screen_name": "iDeanRivers",
        #     "tweet_id":"12345",
        #     "season":1
        # })

        # #---------------------------------------------------
        # tweet_str_4 = "@Halo_Stats_Bot nyke espy:bad query"

        # #---------------------------------------------------

    def test_performance(self):

        # Grunt
        res_1 = determine_performace(0.5)
        self.assertEqual(res_1,"Grunt")
        
        # UNSC
        res_2 = determine_performace(0.6)
        self.assertEqual(res_2,"UNSC Foot Soldier")
        
        # Covenenant
        res_3 = determine_performace(0.9)
        self.assertEqual(res_3,"Covenant Elite")
        
        # Brute
        res_4 = determine_performace(1.1)
        self.assertEqual(res_4,"Brute")
        
        # Honor Guard
        res_5 = determine_performace(2.1)
        self.assertEqual(res_5,"Honor Guard")
        
        # Master Chief
        res_6 = determine_performace(2.5)
        self.assertEqual(res_6,"Master Chief")

    def test_process_matches_data(self):
        dummy_data = {
                            "data": [
                                {
                                "ranked": "true",
                                "id": "456e1287-f9c6-4b5c-b375-24f5b2ddd34d",
                                "details": {
                                    "category": {
                                    "name": "Slayer",
                                    "asset": {
                                        "id": "c2d20d44-8606-4669-b894-afae15b3524f",
                                        "version": "9eee25fe-39db-493f-ab0d-d95403df66b8",
                                        "thumbnail_url": "https://avtqvzpooapfgoykpwxd.supabase.in/storage/v1/object/public/infinite-assets/ugcgamevariants/slayer.png"
                                    }
                                    },
                                    "map": {
                                    "name": "Bazaar",
                                    "asset": {
                                        "id": "298d5036-cd43-47b3-a4bd-31e127566593",
                                        "version": "bca803f0-4447-4a5a-aef4-b31e4be77076",
                                        "thumbnail_url": "https://avtqvzpooapfgoykpwxd.supabase.in/storage/v1/object/public/infinite-assets/maps/bazaar.png"
                                    }
                                    }
                                },
                                "teams": {
                                    "enabled": "true",
                                    "scoring": "true",
                                    "details": {
                                    "id": 1,
                                    "name": "Cobra",
                                    "emblem_url": "https://avtqvzpooapfgoykpwxd.supabase.in/storage/v1/object/public/infinite-assets/teams/team-cobra.png"
                                    }
                                },
                                "stats": {
                                    "summary": {
                                    "kills": 14,
                                    "deaths": 14,
                                    "assists": 4,
                                    "betrayals": 0,
                                    "suicides": 0,
                                    "vehicles": {
                                        "destroys": 0,
                                        "hijacks": 0
                                    },
                                    "medals": 6
                                    },
                                    "damage": {
                                    "taken": 4267,
                                    "dealt": 4653
                                    },
                                    "shots": {
                                    "fired": 475,
                                    "landed": 266,
                                    "missed": 209,
                                    "accuracy": 56
                                    },
                                    "rounds": {
                                    "won": 0,
                                    "lost": 1,
                                    "tied": 0
                                    },
                                    "breakdowns": {
                                    "kills": {
                                        "melee": 1,
                                        "grenades": 0,
                                        "headshots": 9,
                                        "power_weapons": 2
                                    },
                                    "assists": {
                                        "emp": 0,
                                        "driver": 0,
                                        "callouts": 0
                                    }
                                    },
                                    "kda": 1.33,
                                    "kdr": 1,
                                    "score": 1600
                                },
                                "rank": 6,
                                "outcome": "loss",
                                "experience": "arena",
                                "played_at": "2021-11-21T19:55:31.170Z",
                                "duration": {
                                    "seconds": 551,
                                    "human": "00h 09m 11s"
                                }
                                }
                            ],
                            "count": 1,
                            "paging": {
                                "page": 1,
                                "previous": "null",
                                "next": "null"
                            },
                            "additional": {
                                "gamertag": "Zeny IC"
                            }
                        }

        response = process_matches_data(dummy_data)
        self.assertIn("Last match stats for '",response)
        self.assertIn("Outcome:",response)
        self.assertIn("Ranked:",response)
        self.assertIn("Kills:",response)
        self.assertIn("Assists:",response)
        self.assertIn("Deaths",response)
        self.assertIn("KDR",response)
        self.assertIn("Game Mode",response)
        self.assertIn("Map",response)
        self.assertIn("Performance",response)

    def test_process_service_record_data(self):
        dummy_data = {
                        "data": {
                                "summary": {
                                "kills": 2750,
                                "deaths": 1903,
                                "assists": 982,
                                "betrayals": 0,
                                "suicides": 3,
                                "vehicles": {
                                    "destroys": 0,
                                    "hijacks": 0
                                },
                                "medals": 1069
                                },
                                "damage": {
                                "taken": 602686,
                                "dealt": 751337,
                                "average": 5525
                                },
                                "shots": {
                                "fired": 64556,
                                "landed": 38674,
                                "missed": 25882,
                                "accuracy": 59.907677055579654
                                },
                                "breakdowns": {
                                "kills": {
                                    "melee": 319,
                                    "grenades": 75,
                                    "headshots": 2128,
                                    "power_weapons": 113
                                },
                                "assists": {
                                    "emp": 0,
                                    "driver": 0,
                                    "callouts": 0
                                },
                                "matches": {
                                    "wins": 83,
                                    "losses": 42,
                                    "left": 9,
                                    "draws": 2
                                }
                                },
                                "kda": 8.63480392156863,
                                "kdr": 1.4450867052023122,
                                "total_score": 349795,
                                "matches_played": 136,
                                "time_played": {
                                "seconds": 89354,
                                "human": "1d 00h 49m 14s"
                                },
                                "win_rate": 61.029411764705884
                            },
                            "additional": {
                                "gamertag": "Falcated"
                            }
                        }

        response = process_service_record_data(dummy_data)
        self.assertIn("Player Service Record for '",response)
        self.assertIn("Kills:",response)
        self.assertIn("Deaths:",response)
        self.assertIn("KDR:",response)
        self.assertIn("Accuracy:",response)
        self.assertIn("Matches Played:",response)
        self.assertIn("Win Rate:",response)
        self.assertIn("Performance:",response)

    def test_fetch_halo_data(self):
        good_username = "nyke%20espy"
        bad_username = "sdofgiroghusgfoih"
        query_types = {
            "none":"",
            "bad":"bad_query",
            "last_match":"last_match",
            "service_record":"service_record"
        }

        # good username, no query type
        response_none = fetch_halo_data(good_username,query_types["none"])
        self.assertEqual(response_none,None)

        # good username, bad query type
        response_good_username_bad_query = fetch_halo_data(good_username,query_types["bad"])
        self.assertEqual(response_good_username_bad_query,None)

        # good username, good query type, last match
        response_good_username_good_query_last_match = fetch_halo_data(good_username,query_types["last_match"])
        self.assertIn("Last match stats for '",response_good_username_good_query_last_match)
        self.assertIn("Outcome:",response_good_username_good_query_last_match)
        self.assertIn("Ranked:",response_good_username_good_query_last_match)
        self.assertIn("Kills:",response_good_username_good_query_last_match)
        self.assertIn("Assists:",response_good_username_good_query_last_match)
        self.assertIn("Deaths",response_good_username_good_query_last_match)
        self.assertIn("KDR",response_good_username_good_query_last_match)
        self.assertIn("Game Mode",response_good_username_good_query_last_match)
        self.assertIn("Map",response_good_username_good_query_last_match)
        self.assertIn("Performance",response_good_username_good_query_last_match)

        # good username, good query type, service_record
        response_good_username_good_query_service_record = fetch_halo_data(good_username,query_types["service_record"])
        self.assertIn("Player Service Record for '",response_good_username_good_query_service_record)
        self.assertIn("Kills:",response_good_username_good_query_service_record)
        self.assertIn("Deaths:",response_good_username_good_query_service_record)
        self.assertIn("KDR:",response_good_username_good_query_service_record)
        self.assertIn("Accuracy:",response_good_username_good_query_service_record)
        self.assertIn("Matches Played:",response_good_username_good_query_service_record)
        self.assertIn("Win Rate:",response_good_username_good_query_service_record)
        self.assertIn("Performance:",response_good_username_good_query_service_record)

        # bad username, no query type
        response_none = fetch_halo_data(bad_username,query_types["none"])
        self.assertEqual(response_none,None)

        # bad username, bad query type
        response_bad_username_bad_query = fetch_halo_data(bad_username,query_types["bad"])
        self.assertEqual(response_bad_username_bad_query,None)

        # bad username, bad query type, last match
        response_bad_username_bad_query_last_match = fetch_halo_data(bad_username,query_types["last_match"])
        self.assertEqual(response_bad_username_bad_query_last_match,None)

        # bad username, good query type, service_record
        response_bad_username_good_query_service_record = fetch_halo_data(bad_username,query_types["service_record"])
        self.assertEqual(response_bad_username_good_query_service_record,None)

        
if __name__ == '__main__':
    unittest.main()
