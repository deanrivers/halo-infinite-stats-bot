import requests
import cfg

headers = cfg.headers

# stats => 'https://cryptum.halodotapi.com/games/hi/stats/players/Zeny%20IC/csrs?season=1' \
# motd => https://cryptum.halodotapi.com/games/hi/motd \
# player matches => https://cryptum.halodotapi.com/games/hi/stats/players/Zeny%20IC/matches \
# Player Service Record => https://cryptum.halodotapi.com/games/hi/stats/players/Zeny%20IC/service-record/global \

query_types = {
    # "stats":"csrs?season=1", # requirements: username, season
    "last_match":"matches", # requirements: username
    "service_record":"service-record/global", # requirements: username
    # "motd": "" # requirements: none
}

def determine_performace(kdr):
    if kdr >= 2.5:
        return "Master Chief"
    if kdr > 2:
        return "Honor Guard"
    if kdr > 1:
        return "Brute"
    if kdr > .8:
        return "Covenant Elite"
    if kdr > .5:
        return "UNSC Foot Soldier"
    if kdr <= .5:
        return "Grunt"

def process_matches_data(data):
    data_slice = data["data"][0]
    halo_username = data["additional"]["gamertag"]
    processed_string = ""

    data_slice_obj = {
        "outcome": str(data_slice["outcome"]),
        "game_mode": str(data_slice["details"]["category"]["name"]),
        "game_map": str(data_slice["details"]["map"]["name"]),
        "kills": str(data_slice["stats"]["summary"]["kills"]),
        "deaths": str(data_slice["stats"]["summary"]["deaths"]),
        "assists": str(data_slice["stats"]["summary"]["assists"]),
        "medals": str(data_slice["stats"]["summary"]["medals"]),
        "accuracy": str(round(data_slice["stats"]["shots"]["accuracy"],2)),
        "kdr": str(round(data_slice["stats"]["kdr"],2)),
        "ranked": str(data_slice["ranked"]),
    }

    halo_performance = determine_performace(float(data_slice_obj["kdr"]))
    processed_string = "Last match stats for '"+ halo_username +"'"+":\n\n" + "Outcome: " +  data_slice_obj["outcome"] + "\n" + "Ranked: " + data_slice_obj["ranked"] + "\n" + "Kills: " + data_slice_obj["kills"] + "\n" + "Assists: " + data_slice_obj["assists"] + "\n" + "Deaths: " + data_slice_obj["deaths"] + "\n" + "KDR: " + data_slice_obj["kdr"] + "\n" "Game Mode: " + data_slice_obj["game_mode"] + "\n" + "Map: " + data_slice_obj["game_map"] + "\n" + "Performance: " + halo_performance

    return processed_string

def process_service_record_data(data):
    data_slice = data["data"]
    halo_username = data["additional"]["gamertag"]
    processed_string = ""

    data_slice_obj = {
        "kills":str(data_slice["summary"]["kills"]),
        "deaths":str(data_slice["summary"]["deaths"]),
        "medals":str(data_slice["summary"]["medals"]),
        "accuracy":str(round(data_slice["shots"]["accuracy"],2)),
        "kdr":str(round(data_slice["kdr"],2)),
        "matches_played":str(data_slice["matches_played"]),
        "win_rate":str(round(data_slice["win_rate"],2))
    }

    halo_performance = determine_performace(float(data_slice_obj["kdr"]))
    processed_string = "Player Service Record for '"+ halo_username +"'"+":\n\n" + "Kills: " +  data_slice_obj["kills"] + "\n" + "Deaths: " + data_slice_obj["deaths"] + "\n" + "\n" + "KDR: " + data_slice_obj["kdr"] + "\n" + "Accuracy: " + data_slice_obj["accuracy"] + "%" + "\n" + "Matches Played: " + data_slice_obj["matches_played"] + "\n" "Win Rate: " + data_slice_obj["win_rate"] + "%" + "\n" + "Performance: " + halo_performance
    
    return processed_string

def fetch_halo_data(username,query_type):
    if query_type not in list(query_types.keys()):
        return None

    halo_dot_api_url = 'https://cryptum.halodotapi.com/games/hi/stats/players'
    url = halo_dot_api_url + '/' + username + '/' + query_types[query_type]

    print('----------')
    print(url)
    print('----------')
    print('')

    try:
        r = requests.get(url,headers=headers)
        data = r.json()
        status_code = r.status_code
        processed_string = None
        print(data)
        print('----------')
        print("status code =>",status_code)
        print('----------')
        print('')        

        if(status_code!=200):
            return None
        
        if not data["data"]:
            return None

    except Exception as e:
        print('----------')
        print("error hitting halo api =>",e)
        print('----------')
        print('')      
        return None   
    try:
        # process the data accroding to the query type. Each result has different keys returned
        if query_type == "last_match":
            processed_string = process_matches_data(data)
        elif query_type == "service_record":
            processed_string = process_service_record_data(data)
            return processed_string
        else:
            return None
        return processed_string
    
    except Exception as e:
        print('----------')
        print("error processing halo data =>",e)
        print('----------')
        print('')        

        return None

