import json
import oauth2 as oauth
import tweepy
import urllib3
from requests.auth import HTTPBasicAuth
import datetime
import cfg

# tweepy
auth = tweepy.OAuthHandler(cfg.consumer_key, cfg.consumer_secret)
auth.set_access_token(cfg.access_token, cfg.access_token_secret)
api = tweepy.API(auth)

consumer = oauth.Consumer(key=cfg.consumer_key, secret=cfg.consumer_secret)
access_token = oauth.Token(key=cfg.access_token, secret=cfg.access_token_secret)
client = oauth.Client(consumer, access_token)

twitter_endpoint = 'https://api.twitter.com/1.1/statuses/mentions_timeline.json?screen_name=HaloStatsBot&since_id='

def parse_tweet(tweet):
    incoming_tweet = tweet['text'].split("@Halo_Stats_Bot")[1]
    incoming_tweet_arr = incoming_tweet.split(":")
    print("Incoming tweet =>",incoming_tweet)
    tweet_id = tweet["id_str"]
    screen_name = tweet['user']['screen_name']
    print("Incoming tweet arr =>",incoming_tweet_arr)
    halo_username_arr = incoming_tweet_arr[0].strip().split()
    halo_username = "%20".join(halo_username_arr)
    incoming_query_type = incoming_tweet_arr[1].strip() if len(incoming_tweet_arr)>1 else None
    season = 1

    print('----------')
    print("Here are the deets:")
    print("Tweet ID",tweet_id)
    print("Twitter username => ",screen_name)
    print("Stripped tweet => ",incoming_tweet)
    print("Halo username => ", halo_username)
    print("Query type => ",incoming_query_type)
    print('----------')
    print('')

    return {
        "halo_username":halo_username,
        "incoming_query_type":incoming_query_type,
        "screen_name":screen_name,
        "tweet_id":tweet_id,
        "season":season
    }

def fetch_mentions(since_id):
    print("since_id",since_id)
    twitter_response, twitter_data = client.request(twitter_endpoint+since_id)

    # access API to get mentions
    tweets = json.loads(twitter_data) 

    return tweets

def create_tweet(string,twitter_user):
    x = datetime.datetime.now()
    dateDay = x.strftime("%x")
    dateTime = x.strftime("%X")

    tweet = "Request from @" + twitter_user + ":\n\n" + string + "\n\n"+dateDay+" "+dateTime


    # tweet = "Hey @" + twitter_user + ", here are the stats for "+ "'"+ halo_username +"'"+" last game:\n\n" + "Outcome: " +  data_slice_obj["outcome"] + "\n" + "Ranked: " + data_slice_obj["ranked"] + "\n" + "Kills: " + data_slice_obj["kills"] + "\n" + "Assists: " + data_slice_obj["assists"] + "\n" + "Deaths: " + data_slice_obj["deaths"] + "\n" + "KDR: " + data_slice_obj["kdr"] + "\n" "Game Mode: " + data_slice_obj["game_mode"] + "\n" + "Map: " + data_slice_obj["game_map"] + "\n" + "Performance: " + halo_performance + "\n\n"+dateDay+" "+dateTime
    print('----------')
    print("Tweet created ->",tweet)
    print('----------')

    return tweet

def post_tweet(string,tweet_ID):
    api.update_status(string,in_reply_to_status_id=tweet_ID)

