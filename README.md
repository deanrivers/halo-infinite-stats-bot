# Halo Infinite Stats Bot

The Halo Infinite Stats Bot uses the [Halo Dot API](https://developers.halodotapi.com/) and the Twitter API to process requests and respond with results.

## Technologies Used

- Python
- Twitter API
- Halo Dot API

## Contributions

I would love for the community to contribute and point out things that may not be perfect in my codebase.

If you would like to make any contribution, please submit a merge request and I will take a look at it :)

## Twitter Account

[Halo Infinite Stats Bot on Twitter](https://twitter.com/Halo_Stats_Bot)